from flask import Flask
from flask_socketio import SocketIO,send

app = Flask(__name__)
app.config['SECRET_KEY'] = 'secret!'
socketio = SocketIO(app)


@socketio.on('message')
def test_message(message):
    print('message : ' + message)
    send(message, broadcast=True)


if __name__ == '__main__':
    socketio.run(app)
